% clear all;

d = 5; %dim of the local Hilbert space=numb of excit+1
%
%remember the basis is |j> ~ a.dag^(j-1)|0>, with |1>=|0>=vac and
%|d>~a.dag^(d-1)|0> the last state; implying that [phi,tau]=1i*Id only
%in the (d-1)X(d-1) submatrix; in particular the last row and col of the
%local operators have to be descarded because the operator a.dag
%creating the states can be applied at most (d-1) times
%
vac = zeros(d,1);vac(1) = 1;
a_ope = full(sparse(1:d-1,2:d,sqrt(1:d-1),d,d));
phi = full((a_ope+a_ope')/sqrt(2));
p = full((a_ope'-a_ope)*1i/sqrt(2));
id=eye(d);
phi2=zeros(d);
for rr=1:d
    for cc=1:d
	if rr==cc,phi2(rr,cc)=0.5*(2*cc-1);end
	if rr>2&&rr==cc+2, phi2(rr,cc)=0.5*sqrt((rr-1)*(rr-2));end
	phi2(cc,rr)=phi2(rr,cc);
    end
end
phi4=zeros(d);
for rr=1:d
    for cc=1:d
	if rr==cc,phi4(rr,cc)=0.25*3*(2*rr^2-2*rr+1);end
	if rr>1&&(rr-1)==(cc+3), phi4(rr,cc)=0.25*sqrt(factorial(rr-4));end
	if rr==cc+2,
	   phi4(rr,cc)=0.25*4*rr*sqrt(factorial(rr-2));
	end
	if rr>1&&(rr-1)==(cc+1), phi4(rr,cc)=-0.25*6*sqrt(factorial(rr-2));end
	phi4(cc,rr)=phi4(rr,cc);
    end
end
p2=zeros(d);
for rr=1:d
    for cc=1:d
	if rr==cc,p2(rr,cc)=0.5*(2*cc-1);end
	if rr>2&&rr==cc+2, p2(rr,cc)=-0.5*sqrt((rr-1)*(rr-2));end
	p2(cc,rr)=p2(rr,cc);
    end
end

