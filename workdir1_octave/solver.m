function [out,ee] = solver(L,mpoX,R,mps0)
%
%this is unstable on octave, but stable on matlab; it seems to be slower
%than the inverse free eigensolver;
%
h = contracttensors(L,3,2,mpoX,4,1);
h = contracttensors(h,5,3,R,3,2);
h = permute(h,[1,5,3,2,6,4]);
h = reshape(h,[numel(mps0),numel(mps0)]);

opts.disp = 0;
opts.v0 = reshape(mps0,[numel(mps0),1]);
[out,ee] = eigs(h,1,'sr',opts);
out = reshape(out,size(mps0));

