function t = initenv(mps,mpo)

N = size(mps,2);
t = cell(N,1);
t{N} = 1;

t{N-1} = contracttensors(conj(mps{end}),3,3,mpo{end},4,3);
t{N-1} = contracttensors(t{N-1},5,5,mps{end},3,3);
t{N-1} = permute(t{N-1},[1 3 5 2 4]); % [top,mid,bottom]

for ii = N-1:-1:2
    t{ii-1} = updateR(mps{ii},t{ii},mpo{ii});
end
