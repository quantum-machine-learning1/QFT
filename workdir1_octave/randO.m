function U = randO(n)

% randn('seed',sum(100*clock));
% rng shuffle;
% RandStream.setGlobalStream(RandStream('mt19937ar','seed',sum(100*clock)));

X = randn(n);
[Q,R] = qr(X);
R = diag(diag(R)./abs(diag(R)));
U = Q*R;
endfunction
