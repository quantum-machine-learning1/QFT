clear all
close all
more off;
N = 10; 
D = 2^3; 
d = 2^3; 
lamb = 1;
numbp = 10;
lambt = lamb*0.5; %=lamb*a^2
mut02 = linspace(-0.1,-0.3,numbp);
svalue = zeros(numbp,1);
fide = zeros(numbp,1);
v = zeros(numbp,1);
delta = 1e-8;
mps0 = createrandommps(N,D,d);
for ii = 1:numbp
    [mps1,~,~] = phi4(N,D,d,mut02(ii)-delta/2,lambt,mps0);
    [mps2,~,v(ii)] = phi4(N,D,d,mut02(ii)+delta/2,lambt,mps1);
    svalue(ii) = susceptibilty(mps1,mps2,delta);
    fide(ii) = abs(overlap(mps1,mps2));
    plotyy(mut02(1:ii),real(v(1:ii)),mut02(1:ii),real(fide(1:ii)));drawnow
    mps0 = mps2;
end
%     plot(lambt./mut2,real(v(1:ii)),'O');drawnow

%     mps = cellpsi{ii};

% susceptibilty(cellpsi{1},cellpsi{ii},listm(1)-counterterm(listm(1),lamb)-listm(ii)+counterterm(listm(ii),lamb));
%     [mps2,~] = phi4(N,D,d,listm(ii)+delta-counterterm(listm(ii)+delta,lamb),lambt,mps1);

% svalue = zeros(size(listm,2)-1,1);
% for ii = 1:size(listm,2)
%         svalue(ii) = susceptibilty(cellpsi{1},cellpsi{ii},abs(listm(ii)-listm(ii+1)));
%         plot(listm(1:ii),real(svalue(1:ii)),'-x');drawnow
% end
% save('GS.mat','cellpsi');
% F = load('GS');

