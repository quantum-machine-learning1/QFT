function [mps,ee,op] = phi4(N,D,d,mut02,lambt,mps)
[mpo,mpophi] = mpohtil(mut02,lambt,N,d);
%
%prepare the normalized initial state in right canonical form (A.A^dag=Id)
%
if isempty(mps),mps = createrandommps(N,D,d);end
mps = prepare(mps);
[mps{1},~]=prepare_onesite(mps{1},'rl');
%
%initialize the environment
%
Renv = initenv(mps,mpo);
Lenv = cell(N,1);
%
%iterate local solution with Lanczos for MPS
%
iter_in = 10;
iter_out = 3;
precision = 1e-5;
sweep = 0;
while 1,
    sweep = sweep+1;
    elist = [];
    for ii = 1:N-1
        if ii==1, Lenv{ii}=1;end
        [mps{ii},ee] = outer_loop(Lenv{ii},mpo{ii},Renv{ii},...
            mps{ii},iter_in,iter_out,precision);            
        [mps{ii},U] = prepare_onesite(mps{ii},'lr');
        mps{ii+1} = contracttensors(U,2,2,mps{ii+1},3,1);
        Lenv{ii+1} = updateL(mps{ii},Lenv{ii},mpo{ii}); 
        elist = [elist,ee];
        disp([ii,ee])
    end
    for ii = N:-1:2
        if ii==N, Renv{ii}=1;end
        [mps{ii},ee] = outer_loop(Lenv{ii},mpo{ii},Renv{ii},...
            mps{ii},iter_in,iter_out,precision);            
        [mps{ii},U] = prepare_onesite(mps{ii},'rl');
        mps{ii-1} = permute(contracttensors(mps{ii-1},3,2,U,2,1),[1 3 2]);
        Renv{ii-1} = updateR(mps{ii},Renv{ii},mpo{ii}); 
        elist = [elist,ee];
        disp([ii,ee])
    end
    if std(elist)/mean(elist)<precision,break;end
end

op = overlap(mps,mpomps(mpophi,mps))/N;
