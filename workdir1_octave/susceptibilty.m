function out = susceptibilty(mps1,mps2,delta)

out = -2*log(overlap(mps1,mps2))/delta^2;
