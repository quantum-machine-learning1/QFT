function out = mpohtil_wrong(mut02,lambt,N,d)
%
%construction powers of phi or p starding from a finite d-dim representation
%of a and a.dag does not give the correct result in the (d,d) entry since
%the higher basis states |>=d> are not taken into account from a and a.dag;
%also the commutation relation cannot be satisfied; so the right way to
%procede is to construct the correct finite dimentionsal representation of
%the power operators and not build them from a and a.dag
%
out = cell(1,N);
d=5;
a_ope = sparse(1:d-1,2:d,sqrt(1:d-1),d,d);
phi = full((a_ope+a_ope')/sqrt(2));
phi2 = phi^2;
p = full((a_ope'-a_ope)*1i/sqrt(2));
id=eye(d); 

ope_loc = 0.5*p^2 + 0.5*mut02*phi^2 + lambt/24*phi^4;
%
%left border
%
aux2(1,1,:,:) = ope_loc+0.5*phi2;
aux2(1,2,:,:) = -phi;
aux2(1,3,:,:) = id;
out{1} = aux2;
%
%right border
%

aux3(1,1,:,:) = id;
aux3(2,1,:,:) = phi;
aux3(3,1,:,:) = ope_loc+0.5*phi2;
out{N} = aux3;
%
%bulk
%
for indN = 2:N-1
    aux=zeros(3,3,d,d);
    aux(3,:,:,:) = aux2(1,:,:,:);
    aux(:,1,:,:) = aux3(:,1,:,:);
    aux(3,1,:,:) = ope_loc+phi2;
    out{indN} = aux;
end
    
