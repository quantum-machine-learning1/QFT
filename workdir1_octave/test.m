%mut02=-0.2;
%mut2=9.744828*0.001;
%mut2-counterterm(mut2,lambt)

clear all;
numbp = 100;
lambt=0.1;
list = linspace(0.001,0.1,numbp);
value = zeros(numbp,1);
for ii=1:numbp
    value(ii) = list(ii)-counterterm(list(ii),lambt); 
    plot(lambt./list(1:ii),value(1:ii),'-o');drawnow
end
