function out = counterterm(mut2,lamb)
%
%TO BE CHECKED
%
% N = 1e+4;
% out = (lamb/2)*sum(1/(2*N)*1./sqrt(mut2+4.*sin(pi.*(1:N)./N).^2));
out = (lamb/2)*(1/pi)*(1/sqrt(mut2+4))*ellipke(2/sqrt(mut2+4));