1; # this is kept to Prevent Octave from thinking that this is a function file:

close all;
clear all;

num_process=2;

a_test_inputs=10*ones(1,2);

tic ();
tt_par= pararrayfun(num_process,@randO,(a_test_inputs));
parallel_elapsed_time = toc ()

