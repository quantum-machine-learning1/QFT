# -*- coding: utf-8 -*-
"""
Created on Sat Mar 29 18:35:02 2014

@author: sgarnero
"""
import tenet as tn
import time
import numpy as np

N=20
D=16
d=16
lambt = 0.6
mut02 = -0.35
iter_in=3
iter_out=3
precision=1e-6
flag='nodisp'

t0=time.time()
out=tn.phi4([],N,D,d,lambt,mut02,iter_in,iter_out,precision,flag)
t1=time.time()
print(t1-t0)