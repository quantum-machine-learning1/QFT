# -*- coding: utf-8 -*-
"""
Created on Wed Mar 19 16:40:01 2014

@author: sgarnero
"""
def iteratephi4(N,D,d,numbp):
    import numpy as np
    import tenet as tn
    import scipy.io as sio
    import time
    
    inputfile = 'mu0_lamb_N16_D8_d16.mat'
    cont_name = 'out_N16'
    
    v1 = np.zeros(numbp)
    v2 = np.zeros(numbp)
    
    lambt_list = np.array([1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0.03,0.02,0.01])
    svalue = np.zeros((numbp,lambt_list.shape[0]))
    
    mat_contents = sio.loadmat(inputfile)
    val = mat_contents[cont_name][:,0]
    mut02_min = val[:]+0.25*val[:]
    mut02_max = val[:]-0.50*val[:]
    mut02_list = np.zeros(lambt_list.shape[0])

    delta = 1e-10
    iter_in = 5;iter_out = 5;precision = 1e-6
    
    for jj in range(lambt_list.shape[0]):
        mps0 = tn.createrandommps(N,D,d)
        mps0,U = tn.prepare(mps0)
        mps0[0],U,DB = tn.prepare_onesite(np.array(mps0[0]),'rl')
        lambt = lambt_list[jj]
        mut02 = np.linspace(mut02_min[jj],mut02_max[jj],numbp) #it is important to start in the symmetry broken phase(low entanglement) and move twd the symmetric phase (high entanglement)
        for ii in range(numbp):
            if jj>0&ii==0:
                mps0 = mpsaux
            t0=time.time()
            mps1,ee,v1[ii] = tn.phi4(mps0,N,D,d,lambt,mut02[ii]-delta/2.0,iter_in,iter_out,precision,'disp')
            t1=time.time()
            print(t1-t0)            
            t0=time.time()
            mps2,eee,v2[ii] = tn.phi4(mps1,N,D,d,lambt,mut02[ii]+delta/2.0,iter_in,iter_out,precision,'nodisp')
            t1=time.time()
            print(t1-t0)            
            svalue[ii,jj] = tn.susceptibilty(mps1,mps2,delta)
#            print jj,ii,svalue[ii,jj]
            mps0 = mps2
            if ii==0:
                mpsaux = mps1
        mut02_list[jj] = mut02[np.argmax(svalue[:,jj])]
        print jj,mut02_list[jj]
        print
            
    output = np.zeros((lambt_list.shape[0],2))
    output[:,0] = mut02_list[:]
    output[:,1] = lambt_list[:]
#    sio.savemat(outputfile, {cont_out:output})
    return svalue, output

N = 50
D = 2**4
d = 2**4
numbp = 10
svalue,output = iteratephi4(N,D,d,numbp)

#import scipy.io as sio
#outputfile = 'mu0_lamb_N0_D8_d16.mat'
#cont_out = 'out_N0'
#sio.savemat(outputfile, {cont_out:output})
# 
#import matplotlib.pyplot as plt
#import numpy as np
#plt.figure()
##plt.plot(svalue[:,0])
#lambt_list = np.array([1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0.03,0.02,0.01])
#plt.plot(output[:,0],lambt_list[:])
#plt.show()



