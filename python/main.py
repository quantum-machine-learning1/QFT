# -*- coding: utf-8 -*-
"""
Created on Wed Mar 19 16:40:01 2014

@author: sgarnero
"""
#it is important to start in the symmetry broken phase(low entanglement) and move twd the symmetric phase (high entanglement)
def iteratephi4(N,D,d,numbp):
    import numpy as np
    import tenet as tn
    import scipy.io as sio
    
    filt=[0,2,4,6,8,9]+[10,11,12,13]
    if N==32:
        inputfile = 'mu0_lamb_N'+str(0)+'_D8_d16.mat'
        cont_name = 'out_N'+str(0)
        outputfile = 'mu0_lamb_N'+str(N)+'_D'+str(D)+'_d'+str(d)+'.mat'
        cont_out = 'out_N'+str(N)     
        mat_contents = sio.loadmat(inputfile)
        val = mat_contents[cont_name][filt,0]
    else:
        inputfile = 'mu0_lamb_N'+str(int(N/2))+'_D'+str(D)+'_d16.mat'
        cont_name = 'out_N'+str(int(N/2))
        outputfile = 'mu0_lamb_N'+str(N)+'_D'+str(D)+'_d'+str(d)+'.mat'
        cont_out = 'out_N'+str(N)
        mat_contents = sio.loadmat(inputfile)
        val = mat_contents[cont_name][:,0]
    
    v1 = np.zeros(numbp)
    v2 = np.zeros(numbp)
    
    lambt_list = np.array([1.0,0.8,0.6,0.4,0.2,0.1,0.05,0.03,0.02,0.01])
    svalue = np.zeros((numbp,lambt_list.shape[0]))
        
    mut02_list = np.zeros(lambt_list.shape[0])

    delta = 1e-10
    iter_in = 5;iter_out = 5;precision = 1e-7
    
    for jj in range(lambt_list.shape[0]):
        lambt = lambt_list[jj]
        if jj<6:
            mut02_in = val[:]+0.2*val[:]
        else:
            mut02_in = 2.0*val[:]
        if jj==0: 
            mut02 = [val[0]+0.1*val[0]] 
        else:
            mut02 = [mut02_list[jj-1]]
        #if N>64:
         #   mut02 = [mut02_in[jj]]
        ii = -1
        while ii<=4:
            ii = ii+1
            if ii==0:
                mps0 = tn.createrandommps(N,D,d)
                mps0,U = tn.prepare(mps0)
                mps0[0],U,DB = tn.prepare_onesite(np.array(mps0[0]),'rl')

            mps1,ee,v1[ii] = tn.phi4(mps0,N,D,d,lambt,mut02[ii]-delta/2.0,iter_in,iter_out,precision,'nodisp')
            mps2,eee,v2[ii] = tn.phi4(mps1,N,D,d,lambt,mut02[ii]+delta/2.0,iter_in,iter_out,precision,'nodisp')
            svalue[ii,jj] = tn.susceptibilty(mps1,mps2,delta)
            print ii,svalue[ii,jj],v1[ii],mut02[ii]
            if ii<1:
                if jj<6:
                    mut02 = np.append(mut02,mut02[ii]+1e-3)
                else:
                    mut02 = np.append(mut02,mut02[ii]+1e-4)
            else:
                if jj<6:
                    if np.abs(v1[ii]-v1[ii-1])<1e-2:
                        mut02 = np.append(mut02,mut02[ii]+1e-2)
                    else:
                        mut02 = np.append(mut02,mut02[ii]+1e-3)
                else:
                    if np.abs(v1[ii]-v1[ii-1])<1e-2:
                        mut02 = np.append(mut02,mut02[ii]+1e-3)
                    else:
                        mut02 = np.append(mut02,mut02[ii]+1e-4)
            mps0 = mps2
        
        while not ((svalue[ii-4,jj]>svalue[ii-3,jj])&(svalue[ii-3,jj]>svalue[ii-2,jj])&(svalue[ii-3,jj]>svalue[ii-1,jj])&(svalue[ii-1,jj]>svalue[ii,jj])&(np.abs(v1[ii])<0.1)):
            ii = ii+1
            mps1,ee,v1[ii] = tn.phi4(mps0,N,D,d,lambt,mut02[ii]-delta/2.0,iter_in,iter_out,precision,'nodisp')
            mps2,eee,v2[ii] = tn.phi4(mps1,N,D,d,lambt,mut02[ii]+delta/2.0,iter_in,iter_out,precision,'nodisp')
            svalue[ii,jj] = tn.susceptibilty(mps1,mps2,delta)
            print ii,svalue[ii,jj],v1[ii],mut02[ii]
            if jj<6:
                if np.abs(v1[ii]-v1[ii-1])<1e-2:
                    mut02 = np.append(mut02,mut02[ii]+1e-2)
                else:
                    mut02 = np.append(mut02,mut02[ii]+1e-3)
            else:
                if np.abs(v1[ii]-v1[ii-1])<1e-2:
                    mut02 = np.append(mut02,mut02[ii]+1e-3)
                else:
                    mut02 = np.append(mut02,mut02[ii]+1e-4)
            mps0 = mps2
        mut02_list[jj] = mut02[np.argmax(svalue[:,jj])]
        print N,jj,mut02_list[jj]    
        print
        
    output = np.zeros((lambt_list.shape[0],2))
    output[:,0] = mut02_list[:]
    output[:,1] = lambt_list[:]
    sio.savemat(outputfile, {cont_out:output})
    return svalue, output

Nlist = [256]
for ii in xrange(len(Nlist)):
    N = Nlist[ii]
    D = 2**2
    d = 2**4
    numbp = 1000
    svalue,output = iteratephi4(N,D,d,numbp)

#import scipy.io as sio
#outputfile = 'mu0_lamb_N0_D8_d16.mat'
#cont_out = 'out_N0'
#sio.savemat(outputfile, {cont_out:output})
# 
#import matplotlib.pyplot as plt
#import numpy as np
#plt.figure()
##plt.plot(svalue[:,0])
#lambt_list = np.array([1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0.03,0.02,0.01])
#plt.plot(output[:,0],lambt_list[:])
#plt.show()



