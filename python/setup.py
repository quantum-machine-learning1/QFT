# -*- coding: utf-8 -*-
"""
Created on Sun Mar 23 15:04:10 2014

@author: sgarnero
"""

from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'Tensor networks app',
  ext_modules = cythonize("TensorNetwork.pyx"),
)