import numpy as np
import scipy.io as sio
N = 20
d = 2**3
D = 2**3
mat_contents1 = sio.loadmat('state1.mat')
mat_contents2 = sio.loadmat('state2.mat')
out1 = mat_contents1['out1']
out2 = mat_contents2['out2']
mps1 = [None]*N
mps2 = [None]*N
for jj in range(N):
    mps1[jj] = np.zeros(np.append(d,out1[jj,1].shape))        
    mps2[jj] = np.zeros(np.append(d,out2[jj,1].shape))        
    for ii in range(d):
        mps1[jj][ii,:,:]=out1[jj,ii]
        mps2[jj][ii,:,:]=out2[jj,ii]

#        time1 = time.time()
#        time2 = time.time()
#        print  'time per step',(time2-time1)

            
#import json
#with open('data.txt', 'w') as outfile:
#  json.dump(mps, outfile)
#np.savetxt("mps.csv", mps)

#import scipy.io
#matfile = 'state.mat'
#scipy.io.savemat(matfile, mdict={'out': mps}, oned_as='row')

#import os
#    matfile1 = 'state1.mat'
#    filepath = os.path.join("/Users/sgarnero/Dropbox/matlab_dropbox/QFT/workdir1/",matfile1)
#    scipy.io.savemat(filepath, mdict={'out1': mps1}, oned_as='row')
#    matfile2 = 'state2.mat'
#    filepath = os.path.join("/Users/sgarnero/Dropbox/matlab_dropbox/QFT/workdir1/",matfile2)
#    scipy.io.savemat(filepath, mdict={'out2': mps2}, oned_as='row')