# -*- coding: utf-8 -*-
"""
Created on Sat Mar 29 10:50:14 2014

@author: sgarnero
"""

import numpy as np
import scipy.io as sio
#import matplotlib.pyplot as plt
#plt.ion()
numbNpt = 4
numbLpt = 10
numbDpt = 1
data = np.zeros((numbNpt,numbLpt,numbDpt))
xlist = np.zeros(numbNpt,)
N = 16
for ii in range(numbNpt):
    N = 2*N
    xlist[ii] = 1.0/N
#    contents_D8 = sio.loadmat('./mu0_lamb_N'+str(N)+'_D8_d16.mat')
#    out_D8 = contents_D8['out_N'+str(N)]
    contents_D8 = sio.loadmat('./data2/mu0_lamb_N'+str(N)+'_D16_d16.mat')
    out_D8 = contents_D8['out_N'+str(N)]
#    contents_D32 = sio.loadmat('./mu0_lamb_N'+str(N)+'_D32_d16.mat')
#    out_D32 = contents_D32['out_N'+str(N)]
    
    filt=[0,2,4,6,8,9]+[10,11,12,13]

#    data[ii,:,0] = out_D8[filt,0]
    data[ii,:,0] = out_D8[:,0]
#    data[ii,:,2] = out_D32[:,0]

#matfile = 'data.mat'
#sio.savemat(matfile, mdict={'data': data}, oned_as='row')

from scipy.optimize import leastsq 

fitfunc = lambda p, x: p[0]*x + p[1] # Target function
errfunc = lambda p, x, y: fitfunc(p, x) - y # Distance to the target function
p0 = [-0.5, 0.0] # Initial guess for the parameters

mut02TDL = np.zeros(data.shape[1],)
for jj in xrange(data.shape[1]):
#    only consider D=32: data[:,jj,2]
    p1, success = leastsq(errfunc, p0[:], args=(xlist, data[:,jj,0]))
    mut02TDL[jj] = fitfunc(p1,0)    

lambt = np.array([1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0.03,0.02,0.01])
lambt = lambt[filt]
#from pylab import plot
#plot(xlist, data[:,0,2], "ro", xlist, fitfunc(p1, xlist), "r-") # Plot of the data and the fit

from scipy.special import ellipk
def func_ct(mut2,lambt):
    out = mut2-(lambt/2.0)*(1.0/np.pi)*(1.0/np.sqrt(mut2+4.0))*ellipk(2.0/np.sqrt(mut2+4.0))
    return out
def func_0(mut2,mut02,lambt):
    out = -mut02 + func_ct(mut2,lambt)
    return out

#from scipy.optimize import brentq
#from scipy.optimize import newton
#for ii in xrange(data.shape[1]):
#    print lambt[ii]/brentq(func_0,lambt[ii]/1000.0,lambt[ii]/1.0,args=(mut02TDL[ii],lambt[ii]))
