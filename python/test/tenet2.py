# -*- coding: utf-8 -*-
"""
Created on Sun Mar 23 15:05:42 2014

@author: sgarnero
"""
#from numba import autojit
import numpy as np
import scipy.linalg.blas
#from scipy import linalg
def tensordot2(a, b, axes=2):
    try:
        iter(axes)
    except:
        axes_a = list(range(-axes, 0))
        axes_b = list(range(0, axes))
    else:
        axes_a, axes_b = axes
    try:
        na = len(axes_a)
        axes_a = list(axes_a)
    except TypeError:
        axes_a = [axes_a]
        na = 1
    try:
        nb = len(axes_b)
        axes_b = list(axes_b)
    except TypeError:
        axes_b = [axes_b]
        nb = 1

    a, b = np.asarray(a), np.asarray(b)
    as_ = a.shape
    nda = len(a.shape)
    bs = b.shape
    ndb = len(b.shape)
    equal = True
    if (na != nb): equal = False
    else:
        for k in range(na):
            if as_[axes_a[k]] != bs[axes_b[k]]:
                equal = False
                break
            if axes_a[k] < 0:
                axes_a[k] += nda
            if axes_b[k] < 0:
                axes_b[k] += ndb
    if not equal:
        raise ValueError("shape-mismatch for sum")

    # Move the axes to sum over to the end of "a"
    # and to the front of "b"
    notin = [k for k in range(nda) if k not in axes_a]
    newaxes_a = notin + axes_a
    N2 = 1
    for axis in axes_a:
        N2 *= as_[axis]
    newshape_a = (-1, N2)
    olda = [as_[axis] for axis in notin]

    notin = [k for k in range(ndb) if k not in axes_b]
    newaxes_b = axes_b + notin
    N2 = 1
    for axis in axes_b:
        N2 *= bs[axis]
    newshape_b = (N2, -1)
    oldb = [bs[axis] for axis in notin]

    at = a.transpose(newaxes_a).reshape(newshape_a)
    bt = b.transpose(newaxes_b).reshape(newshape_b)
#    res = scipy.linalg.blas.dgemm(alpha=1.0, a=at, b=bt)
    res = np.dot(at, bt)
#    rr = at.shape[0]
#    print rr
#    cc = bt.shape[1]
#    print cc,N2
#    res = np.zeros((rr,cc))
#    for row in range(rr):
#        for col in range(cc):
#            for ii in range(N2):
#                res[row,col] += at[row,N2]*bt[N2,col]
    return res.reshape(olda + oldb)

#
# FAST SVD        
#function [U,S,V]=svd2(T)
def svd2(T):
    m,n = T.shape
    if m>=n:
        U,S,V = np.linalg.svd(T,0)
        return U,np.diag(S),V
    else: 
        V,S,U = np.linalg.svd(T.transpose().conj(),0)
        return U.conj().transpose(),np.diag(S),V.conj().transpose()
#
#function [Cright]=updateCright(Cright,B,X,A)
#
def updateCright(Cright,B,X,A):
    if X==[]: 
        X = np.reshape(np.eye(B.shape[0],B.shape[0]),(1,1,2,2),'F')
    Cright = tensordot2(A,Cright,axes=(2,2)) 
    Cright = tensordot2(X,Cright,axes=([1,3],[3,0]))
    Cright = tensordot2(B.conj(),Cright,axes=([0,2],[1,3]))
    return Cright
#
#function out = updateR(mpsX,t,mpoX)
#    
def updateR(mpsX,t,mpoX):
    out = tensordot2(mpsX.conj(),t,axes=(2,0))
    out = tensordot2(out,mpoX,axes=([0,2],[2,1]))
    out = tensordot2(out,mpsX,axes=([3,1],[0,2]))
    return out
#            
#            function out = updateL(mpsX,t,mpoX)
#
def updateL(mpsX,t,mpoX):
    out = tensordot2(mpsX.conj(),t,axes=(1,0))
    out = tensordot2(out,mpoX,axes=([0,2],[2,0]))
    out = tensordot2(out,mpsX,axes=([1,3],[1,0]))
    return out
#
#function t = initenv(mps,mpo)
#
def initenv(mps,mpo):
    N = len(mps)
    t = [None]*N
    t[-1] = np.reshape(1,(1,1,1))    
    t[-2] = tensordot2(mps[-1].conj(),mpo[-1],axes=(0,2))
    t[-2] = tensordot2(t[-2],mps[-1],axes=(4,0))
    t[-2] = np.squeeze(np.ndarray.transpose(t[-2],0,2,4,1,3,5))
    for ii in xrange(N-2,0,-1):
        t[ii-1] = updateR(mps[ii],t[ii],mpo[ii])
    return t
#
#PREPARE ONE_SITE
#function [B,U,DB]=prepare_onesite(A,direction)
def prepare_onesite(A,direction):
    d,D1,D2 = A.shape 
    if direction=='lr':
        A = np.reshape(A,(d*D1,D2),'F') 
        B,S,U = svd2(A) 
        DB = S.shape[0]
        B = np.reshape(B,(d,D1,DB),'F') 
        U = np.dot(S,U)
        return B,U,DB
    elif direction=='rl':
        A = np.ndarray.transpose(A,1,0,2)
        A = np.reshape(A,(D1,d*D2),'F') 
        U,S,B = svd2(A)
        DB = S.shape[0]
        B = np.reshape(B,(DB,d,D2),'F') 
        B = np.ndarray.transpose(B,1,0,2)
        U = np.dot(U,S)
        return B,U,DB

#
# PREPARE CANONICAL FORM
#function [mps,U]=prepare(mps) 
def prepare(mps):
    N = len(mps)
    for ii in xrange(N-1,0,-1):
        mps[ii],U,DB = prepare_onesite(mps[ii],'rl')
        mps[ii-1] = tensordot2(mps[ii-1],U,axes=(2,0))
    return mps,U
#
#GENERATE A RANDOM ORTHOGONAL MATRIX
#
def randO(n):
    X = np.random.randn(n,n)
    Q,R = np.linalg.qr(X)
    R = np.divide(np.diag(np.diag(R)),np.absolute(np.diag(R)))
    U = np.dot(Q,R)
    return U

#
#GENERATE A RANDOM MPS STATE WITH OBC
#mps(ii).shape=[d,Dl,Dr]
def createrandommps(N,D,d):
    mps = [None]*N
    mps[0] = np.random.randn(d,1,D)/np.sqrt(D)
    mps[-1] = np.random.randn(d,D,1)/np.sqrt(D)
    ind = 1
    while ind < N:
        u = randO(D*d)
        u = np.reshape(u,(d,D,d,D),'F')
        u = np.ndarray.transpose(u,0,1,3,2)
        if ind+d-1<N-1:
            dd = -1
            for ii in xrange(ind,ind+d):
                dd = dd+1
                mps[ii] = u[:,:,:,dd]
        else:
            dd = -1
            for ii in xrange(ind,N-1):
                dd = dd+1
                mps[ii] = u[:,:,:,dd]
        ind = ind+d
    return mps
#
#function overlap=overlap(mps1,mps2)
#
#%evaluates <MPS1|MPS2> [hence the norm is sqrt(overlap)] for open boundary conditions
#%the complexity is O(N*D^3*d)
#%!!!!REMEMBER TO PROVIDE INPUT STATE THAT ARE NORMALIZED!!!!
def overlap(mps1,mps2):
    N1 = len(mps1)
    d = mps1[0].shape[0]
    overlap = np.reshape(1,(1,1,1),'F')
    X = np.eye(d,d)
    X = np.reshape(X,(1,1,d,d),'F') 
    for j in xrange(N1-1,-1,-1):
        overlap = updateCright(overlap,mps1[j],X,mps2[j]) 
    return overlap
#
#function v = matvectn(L,R,mpox,v0)
#
def matvectn(L,R,mpox,v0):
    v = tensordot2(L,v0,axes=(2,1))
    v = tensordot2(v,mpox,axes=([1,2],[0,3]))
    v = tensordot2(v,R,axes=([2,1],[1,2]))
    v = np.ndarray.transpose(v,1,0,2)
    return v    
#
#GENERATE MPO REPRESENTATION OF THE HAMILTONIAN FOR THE PHI4 MODEL
#
def mpohtil(mu,lamb,N,d):
    out = [None]*N
    mpophi = [None]*N
    id = np.eye(d,d)
    phi = np.zeros((d,d))
    phi2 = np.zeros((d,d))
    phi4 = np.zeros((d,d))
    p2 = np.zeros((d,d))
    for jp in xrange(d):
        for j in xrange(d):
            if j==jp:
                jv = j+1
                phi2[j,jp] = 0.5*(2*jv-1)
                phi4[j,jp] = 0.25*3*(2*jv**2-2*jv+1)
                p2[j,jp] = 0.5*(2*jv-1)
            if (j-1)==jp: 
                jv = j+1
                phi[j,jp] = (1/np.sqrt(2))*np.sqrt(jv-1)
            if (j-1)==(jp+1): 
                jv = j+1
                phi2[j,jp] = 0.5*np.sqrt((jv-1)*(jv-2));
                phi4[j,jp] = -0.25*6*np.sqrt((jv-1)*(jv-2));
                p2[j,jp] = -0.5*np.sqrt((jv-1)*(jv-2));
            if (j-1)==(jp+3): 
                jv = j+1
                phi4[j,jp] = 0.25*np.sqrt((jv-1)*(jv-2)*(jv-3)*(jv-4))
            if j==(jp+2):
                jv = j+1
                phi4[j,jp] = jv*np.sqrt((jv-1)*(jv-2))
            phi[jp,j] = phi[j,jp];
            phi2[jp,j] = phi2[j,jp]
            phi4[jp,j] = phi4[j,jp]
            p2[jp,j] = p2[j,jp]
    ope_loc = 0.5*p2 + 0.5*mu*phi2 + (lamb/24)*phi4
#left border
    aux2 = np.zeros((1,3,d,d))
    aux2[0,0,:,:] = ope_loc+0.5*phi2
    aux2[0,1,:,:] = -phi
    aux2[0,2,:,:] = id
    out[0] = aux2
#right border
    aux3 = np.zeros((3,1,d,d))
    aux3[0,0,:,:] = id
    aux3[1,0,:,:] = phi
    aux3[2,0,:,:] = ope_loc+0.5*phi2
    out[-1] = aux3
#bulk
    for indN in xrange(1,N-1):
        aux = np.zeros((3,3,d,d))
        aux[2,:,:,:] = aux2[0,:,:,:]
        aux[:,0,:,:] = aux3[:,0,:,:]
        aux[2,0,:,:] = ope_loc+phi2
        out[indN] = aux
#%prepare the mpo for the phi observable
    del aux, aux2, aux3
#%left border
    aux2 = np.zeros((1,2,d,d))
    aux2[0,0,:,:] = phi
    aux2[0,1,:,:] = id
    mpophi[0] = aux2
#%right border
    aux3 = np.zeros((2,1,d,d))
    aux3[0,0,:,:] = id
    aux3[1,0,:,:] = phi
    mpophi[-1] = aux3
#%bulk
    for indN in xrange(1,N-1):
        aux = np.zeros((2,2,d,d))
        aux[1,:,:,:] = aux2[0,:,:,:]
        aux[:,0,:,:] = aux3[:,0,:,:]
        aux[1,0,:,:] = phi
        mpophi[indN] = aux
    return out,mpophi
#
#    function [mpsm,ee] = inner_loop(V1,mpo1,U1,mpsm,e0,m)
#
def inner_loop(V1,mpo1,U1,mpsm,e0,m):
    d,Dl,Dr = mpsm.shape
    vv = np.zeros((Dl*Dr*d,m+1))
    z1 = np.zeros((d,Dl,Dr,m))
    vv[:,0] = np.reshape(mpsm,(Dl*Dr*d))
    for ii in xrange(m):
        z1[:,:,:,ii] = np.reshape(vv[:,ii],(d,Dl,Dr))
        vv[:,ii+1] = np.reshape(matvectn(V1,U1,mpo1,z1[:,:,:,ii]),(Dl*Dr*d))-e0*vv[:,ii]
    zz, aux = np.linalg.qr(vv,mode='reduced')
    mm = zz.shape[1]
    vv2 = np.zeros((Dl*Dr*d,mm))
    z2 = np.reshape(zz,(d,Dl,Dr,mm))
    for jj in xrange(mm):
        vv2[:,jj] = np.reshape(matvectn(V1,U1,mpo1,z2[:,:,:,jj]),(Dl*Dr*d))-e0*zz[:,jj]
    Am = np.dot(zz.conj().transpose(),vv2)
    eigval,eigvec = np.linalg.eigh(Am)
    mu = eigval[0]
    nu = eigvec[:,0]     
    ee = e0+mu
    ee = ee.real
    mpsm = np.reshape(np.dot(zz,nu),(d,Dl,Dr))
    return mpsm,ee
#    
#    function [mps_out,ee_out,list] = outer_loop(V1,mpo1,U1,mpsm,m_in,m_out,precision)
#
def outer_loop(V1,mpo1,U1,mpsm,m_in,m_out,precision): 
    list1 = np.zeros(100)
    mps = [None]*100
    list1[0] = tensordot2(mpsm.conj(),matvectn(V1,U1,mpo1,mpsm),axes=([0,1,2],[0,1,2]))
    mps[0] = mpsm
    ii = 0
    for jj in xrange(m_out):
        ii = ii+1
        mps[ii],list1[ii] = inner_loop(V1,mpo1,U1,mps[ii-1],list1[ii-1],m_in)
    
    while ((np.std(list1[ii-m_out:ii])/abs(np.mean(list1[ii-m_out:ii])) > precision) & (ii<98)):
        ii = ii+1
        mps[ii],list1[ii] = inner_loop(V1,mpo1,U1,mps[ii-1],list1[ii-1],m_in)
    mps_out = mps[ii]
    ee_out = list1[ii]
    if list1[ii]>list1[0]:
        mps_out = mps[0]
        ee_out = list1[0]
    return mps_out,ee_out
#
#function [out,ee] = solver(L,mpoX,R,mps0)
#%this is unstable on octave, but stable on matlab; it seems to be slower
#%than the inverse free eigensolver;
#%
#def solver(L,mpoX,R,mps0):
#    h = contracttensors(L,3,1,mpoX,4,0)
#    h = contracttensors(h,5,2,R,3,1)
#    h = np.ndarray.transpose(h,2,0,4,3,1,5)
#    h = np.reshape(h,(np.prod(mps0.shape),np.prod(mps0.shape)),'F')
#    
#    vv0 = np.reshape(mps0,(np.prod(mps0.shape)),'F')
#    ee,out = eigsh(h,1,which='SA',v0=vv0)
#    out = np.reshape(out,(mps0.shape),'F')
#    return out,ee[0]




        
