# -*- coding: utf-8 -*-
"""
Created on Wed Mar 19 16:40:01 2014

@author: sgarnero
"""
def iteratephi4(N,D,d,numbp):
    import numpy as np
    import tenet as tn
    import scipy.io as sio
    
    if N==32:
#        outputfile = 'f_lambt_N'+str(N)+'_D'+str(D)+'_d'+str(d)+'.mat'
#        cont_out = 'out_N'+str(N)
        c_in = 1500
        c_fin = 500        
    else:
        inputfile = 'f_lamb_N'+str(int(N/2))+'_D'+str(D)+'_d16.mat'
        cont_name = 'out_N'+str(int(N/2))
#        outputfile = 'f_lamb_N'+str(N)+'_D'+str(D)+'_d'+str(d)+'.mat'
#        cont_out = 'out_N'+str(N)
        mat_contents = sio.loadmat(inputfile)
        val = mat_contents[cont_name][:,0]
        c_in = val[:]+0.50*val[:]
        c_fin = val[:]-0.50*val[:]
        
    lambt = np.array([1.0,0.8,0.6,0.4,0.2,0.1,0.05,0.04,0.03,0.02,0.01])
    svalue = np.zeros((numbp,lambt.shape[0]))

#    f_list = np.zeros(lambt.shape[0])
            
    v1 = np.zeros(numbp)
#    v2 = np.zeros(numbp)
    
    delta = 1e-10
    iter_in = 5;iter_out = 5;precision = 1e-7    
    for jj in range(lambt.shape[0]):
        mps0 = tn.createrandommps(N,D,d)
        mps0,U = tn.prepare(mps0)
        mps0[0],U,DB = tn.prepare_onesite(np.array(mps0[0]),'rl')
        if jj>0:
            c_in = c_in + 0.05*c_in
            c_fin = c_in - 100
        c_list = np.linspace(c_in,c_fin,numbp)
        for ii in range(numbp):
#            if (jj>0)&(ii==0):
#                mps0 = mpsaux
            mut02 = tn.func_ct(lambt[jj]/c_list[ii],lambt[jj])
            mps1,ee,v1[ii] = tn.phi4(mps0,N,D,d,lambt[jj],mut02-delta/2,iter_in,iter_out,precision,'nodisp')
#            mps2,eee,v2[ii] = tn.phi4(mps1,N,D,d,lambt[jj],mut02+delta/2,iter_in,iter_out,precision,'nodisp')
#            svalue[ii,jj] = tn.susceptibilty(mps1,mps2,delta)
            print jj,ii
            print v1[ii],c_list[ii],mut02
            print
            mps0 = mps1
#            if ii==0:
#                mpsaux = mps1
#        f_list[jj] = c_list[np.argmax(svalue[:,jj])]
        print        
#        print jj,f_list[jj]    
        print
        
#    output = np.zeros((lambt.shape[0],2))
#    output[:,0] = f_list[:]
#    output[:,1] = lambt[:]
#    sio.savemat(outputfile, {cont_out:output})
    return svalue, output

Nlist = [32,64,128,256,512]
for ii in xrange(len(Nlist)):
    N = Nlist[ii]
    D = 2**4
    d = 16
    numbp = 100
    svalue,output = iteratephi4(N,D,d,numbp)

#import scipy.io as sio
#outputfile = 'mu0_lamb_N0_D8_d16.mat'
#cont_out = 'out_N0'
#sio.savemat(outputfile, {cont_out:output})
# 
#import matplotlib.pyplot as plt
#import numpy as np
#plt.figure()
##plt.plot(svalue[:,0])
#lambt = np.array([1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0.03,0.02,0.01])
#plt.plot(output[:,0],lambt[:])
#plt.show()



