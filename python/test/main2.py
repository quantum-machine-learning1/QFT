# -*- coding: utf-8 -*-
"""
Created on Mon Apr  7 19:25:50 2014

@author: sgarnero
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Mar 19 16:40:01 2014

@author: sgarnero
"""
def iteratephi4(N,D,d,numbp):
    import numpy as np
    import tenet as tn
    import scipy.io as sio
    
    lambt = np.array([1.0,0.8,0.6,0.4,0.2,0.1,0.05,0.04,0.03,0.02,0.01])
            
    v1 = np.zeros(numbp)
    
    iter_in = 5;iter_out = 5;precision = 1e-7    
    for jj in range(lambt.shape[0]):
        mps0 = tn.createrandommps(N,D,d)
        mps0,U = tn.prepare(mps0)
        mps0[0],U,DB = tn.prepare_onesite(np.array(mps0[0]),'rl')
        mut02 = np.linspace(-0.5,-0.3,numbp)
        for ii in range(numbp):
            mps1,ee,v1[ii] = tn.phi4(mps0,N,D,d,lambt[jj],mut02[ii],iter_in,iter_out,precision,'nodisp')
            print jj,ii
            print v1[ii],mut02[ii]
            print
            mps0 = mps1

    return svalue, output

Nlist = [32,64,128,256,512]
for ii in xrange(len(Nlist)):
    N = Nlist[ii]
    D = 2**3
    d = 16
    numbp = 20
    svalue,output = iteratephi4(N,D,d,numbp)

