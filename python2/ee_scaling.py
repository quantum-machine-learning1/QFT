# -*- coding: utf-8 -*-
"""
Created on Fri Apr 11 09:54:01 2014

@author: sgarnero
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Apr 10 16:19:00 2014

@author: sgarnero
"""
import tenet as tn
import numpy as np
from matplotlib.pylab import plot
D = 2**3
d = 18
iter_in = 5;iter_out = 5;precision = 1e-7
lambt = 1.0
mut02 = -0.50

numbpt = 4
ee = np.zeros(numbpt)
bb = np.zeros(numbpt)
x_list = np.zeros(numbpt)

N = 16
for ii in xrange(numbpt):
    x_list[ii] = 1.0/N
    mps,ee[ii],_ = tn.phi4([],N,D,d,lambt,mut02,iter_in,iter_out,precision,'nodisp')
    N = 2*N
    print ii,ee[ii]
    
from scipy.optimize import leastsq 

fitfunc = lambda p, x: p[0]*x + p[1]
errfunc = lambda p, x, y: fitfunc(p, x) - y 
p0 = [-0.5, 0.0]
p1, success = leastsq(errfunc, p0[:], args=(x_list, ee))
eeTDL = fitfunc(p1,0)    

fitfunc = lambda p, x: p[0]*x 
errfunc = lambda p, x, y: fitfunc(p, x) - y
p = [1.0]
p1, success = leastsq(errfunc, p[:], args=(x_list, ee-eeTDL))
print p1
plot(x_list, ee-eeTDL, "ro", x_list, fitfunc(p1, x_list), "r-") # Plot of the data and the fit
