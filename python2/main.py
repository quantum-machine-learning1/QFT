# -*- coding: utf-8 -*-
"""
Created on Thu Apr 10 16:19:00 2014

@author: sgarnero
"""
import tenet as tn
import numpy as np
from matplotlib.pylab import plot
from scipy.optimize import leastsq 

D = 2**3
d = 18
iter_in = 5;iter_out = 5;precision = 1e-7

lambt = 1.0
N00 = 18
N0 = N00
numbM = 30 #mut02
numbN = 1

mps0 = tn.createrandommps(N0,D,d)
mps0,_ = tn.prepare(mps0)
mps0[0],_,_ = tn.prepare_onesite(np.array(mps0[0]),'rl')

ee = np.zeros((numbN*3,numbM))
bb = np.zeros((numbN*3,numbM))
b = np.zeros((numbN*3,numbM))
Left = np.zeros((numbM,3))
Right = np.zeros((numbM,3))
b_list = []
N_list = []
mut02 = [-0.51]
 
x_list = np.zeros(3*numbN)
#==============================================================================
# loop over mut02 values
#==============================================================================
for ii in xrange(numbM):
    jjj = 0
#==============================================================================
# loop over system size
#==============================================================================
    for jj in xrange(numbN):
        print ii,jj,N0,mut02[ii]
        x_list[jjj] = 1.0/N0
        mps1,ee[jjj,ii],b[jjj,ii],bb[jjj,ii] = tn.phi4(mps0,N0,D,d,lambt,mut02[ii],iter_in,iter_out,precision,'nodisp')
        mpsaux = mps1
        
        mps0 = tn.merge_mps(mps1)        
        N = N0+2
        x_list[jjj+1] = 1.0/N
        mps1,ee[jjj+1,ii],b[jjj+1,ii],bb[jjj+1,ii]= tn.phi4(mps0,N,D,d,lambt,mut02[ii],iter_in,iter_out,precision,'nodisp')

        mps0 = tn.merge_mps(mps1)        
        N = N+2
        x_list[jjj+2] = 1.0/N
        mps1,ee[jjj+2,ii],b[jjj+2,ii],bb[jjj+2,ii] = tn.phi4(mps0,N,D,d,lambt,mut02[ii],iter_in,iter_out,precision,'nodisp')
    
        if jj<numbN-1:
            N0 = N*2
#            mps0 = tn.merge_mps(mps1)        
            mps0 = []
        else:
            N0 = N00
            mps0 = mpsaux
            
        jjj = jjj+3
#==============================================================================
# find the scaling in N of the energy and phi2
#==============================================================================
    fitfunc = lambda p, x: p[0]*x + p[1] 
    errfunc = lambda p, x, y: fitfunc(p, x) - y 
    p0 = [-0.5, 0.0] 
    p1, success = leastsq(errfunc, p0[:], args=(x_list, ee[:,ii]))
    eeTDL = fitfunc(p1,0.0) #thermodynamic limit   
    p1, success = leastsq(errfunc, p0[:], args=(x_list, bb[:,ii]))
    bbTDL = fitfunc(p1,0.0) #thermodynamic limit   
    fitfunc = lambda p, x: p[0]*x 
    errfunc = lambda p, x, y: fitfunc(p, x) - y 
    p = [1.0] 
    A, successA = leastsq(errfunc, p[:], args=(x_list, ee[:,ii]-eeTDL))
    p = [1.0] 
    B, successB = leastsq(errfunc, p[:], args=(x_list, bb[:,ii]-bbTDL))
#==============================================================================
# renormalize ee[:,ii] and bb[:,ii]
#==============================================================================
    ee[:,ii] = ee[:,ii]-A*x_list
    bb[:,ii] = bb[:,ii]-B*x_list

    Left[ii,0] = (bb[1,ii]-bb[0,ii])/(ee[1,ii]-ee[0,ii])
    Right[ii,0] = (bb[2,ii]-bb[1,ii])/(ee[2,ii]-ee[1,ii])
    print (Left[ii,0]-Right[ii,0])
    print
    if numbN>1:
        Left[ii,1] = (bb[1+3,ii]-bb[0+3,ii])/(ee[1+3,ii]-ee[0+3,ii])
        Right[ii,1] = (bb[2+3,ii]-bb[1+3,ii])/(ee[2+3,ii]-ee[1+3,ii])
    if numbN>2:
        Left[ii,2] = (bb[1+6,ii]-bb[0+6,ii])/(ee[1+6,ii]-ee[0+6,ii])
        Right[ii,2] = (bb[2+6,ii]-bb[1+6,ii])/(ee[2+6,ii]-ee[1+6,ii])
#==============================================================================
#        this does not work
#     print np.abs((bb[1,:]-bb[0,:])/(ee[1,:]-ee[0,:]) - (bb[2,:]-bb[1,:])/(ee[2,:]-ee[1,:]))
#     print np.abs((bb[4,:]-bb[3,:])/(ee[4,:]-ee[3,:]) - (bb[5,:]-bb[4,:])/(ee[5,:]-ee[4,:]))
#     print np.abs((bb[7,:]-bb[6,:])/(ee[7,:]-ee[6,:]) - (bb[8,:]-bb[7,:])/(ee[8,:]-ee[7,:]))
#==============================================================================
    mut02 = np.append(mut02,mut02[ii]+0.25*1e-2)    
    
    
