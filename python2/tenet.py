# -*- coding: utf-8 -*-
"""
Created on Sun Mar 23 15:05:42 2014

@author: sgarnero
"""

import numpy as np
from scipy import linalg
from scipy import special
from scipy import optimize
#from decimal import *
def _force_forder(x):
    """
    Converts arrays x to fortran order. Returns
    a tuple in the form (x, is_transposed).
    """
    if x.flags.c_contiguous:
        return (x.T, True)
    else:
        return (x, False)
        
def dot2(A,B):
    """
    Uses blas libraries directly to perform dot product
    """
    A, trans_a = _force_forder(A)
    B, trans_b = _force_forder(B)
    gemm_dot = linalg.get_blas_funcs("gemm", arrays=(A,B))
    
    # gemm is implemented to compute: C = alpha*AB  + beta*C
    return gemm_dot(alpha=1.0, a=A, b=B, trans_a=trans_a, trans_b=trans_b)

def contracttensors(X,numindX,indX,Y,numindY,indY):
    Xsize = np.ones(numindX) 
    Xsize[0:len(X.shape)] = X.shape 
    Ysize = np.ones(numindY)
    Ysize[0:len(Y.shape)] = Y.shape
    
    indXl = xrange(numindX) 
    indXl = np.delete(indXl,indX)
    indYr = xrange(numindY) 
    indYr = np.delete(indYr,indY)
    
    sizeXl = Xsize[indXl] 
    sizeX = Xsize[indX]
    sizeYr = Ysize[indYr] 
    sizeY = Ysize[indY]
    
    if np.prod(sizeX)!=np.prod(sizeY):
        print 'indX and indY are not of same dimension.'
    
    label = 0    
    if not list(indYr):
        if not list(indXl):
            X = np.ndarray.transpose(X,indX) 
            X = np.reshape(X,np.append(1,np.prod(sizeX)))
            Y = np.ndarray.transpose(Y,indY)
            Y = np.reshape(Y,np.append(np.prod(sizeY),1))
            X = np.dot(X,Y)
            label = 1
            Xsize = 1
            return X
        else:
            X = np.ndarray.transpose(X,np.append(indXl,indX))
            X = np.reshape(X,(np.prod(sizeXl),np.prod(sizeX)))
            Y = np.ndarray.transpose(Y,indY) 
            Y = np.reshape(Y,np.prod(sizeY))
            X = np.dot(X,Y) 
            Xsize = [Xsize[i] for i in indXl]
            X = np.reshape(X,Xsize)
            label = 1
            return X
    
    if label==0:
        X = np.ndarray.transpose(X,np.append(indXl,indX))
        X = np.reshape(X,(np.prod(sizeXl),np.prod(sizeX)))
#==============================================================================
#         X = np.reshape(X,(sizeXl,np.prod(sizeX)))
#==============================================================================
        
        Y = np.ndarray.transpose(Y,np.append(indY,indYr)) 
        Y = np.reshape(Y,(np.prod(sizeY),np.prod(sizeYr)))
        
        X = np.dot(X,Y) 
#==============================================================================
#         X = dot2(X,Y) 
#==============================================================================
        Xsize = [Xsize[i] for i in indXl]+[Ysize[i] for i in indYr]
        numindX = len(Xsize) 
        X = np.reshape(X,Xsize)
        return X

# FAST SVD        
#function [U,S,V]=svd2(T)
def svd2(T):
    m,n = T.shape
    if m>=n:
        U,S,V = linalg.svd(T,0)
        return U,np.diag(S),V
    else: 
        V,S,U = linalg.svd(T.transpose().conj(),0)
        return U.conj().transpose(),np.diag(S),V.conj().transpose()
#
#function [Cright]=updateCright(Cright,B,X,A)
#
def updateCright(Cright,B,X,A):
    Cright = np.tensordot(A,Cright,axes=(2,2)) 
    Cright = np.tensordot(X,Cright,axes=([1,3],[3,0]))
    Cright = np.tensordot(B.conj(),Cright,axes=([0,2],[1,3]))
    return Cright
#
#function out = updateR(mpsX,t,mpoX)
#    
def updateR(mpsX,t,mpoX):
    out = np.tensordot(mpsX.conj(),t,axes=(2,0))
    out = np.tensordot(out,mpoX,axes=([0,2],[2,1]))
    out = np.tensordot(out,mpsX,axes=([3,1],[0,2]))
    return out
#            
#            function out = updateL(mpsX,t,mpoX)
#
def updateL(mpsX,t,mpoX):
    out = np.tensordot(mpsX.conj(),t,axes=(1,0))
    out = np.tensordot(out,mpoX,axes=([0,2],[2,0]))
    out = np.tensordot(out,mpsX,axes=([1,3],[1,0]))
    return out
#
#function t = initenv(mps,mpo)
#
def initenv(mps,mpo):
    N = len(mps)
    t = [None]*N
    t[-1] = np.reshape(1,(1,1,1))    
    t[-2] = np.tensordot(mps[-1].conj(),mpo[-1],axes=(0,2))
    t[-2] = np.tensordot(t[-2],mps[-1],axes=(4,0))
    t[-2] = np.squeeze(np.ndarray.transpose(t[-2],0,2,4,1,3,5))
    for ii in xrange(N-2,0,-1):
        t[ii-1] = updateR(mps[ii],t[ii],mpo[ii])
    return t
#
#PREPARE ONE_SITE
#function [B,U,DB]=prepare_onesite(A,direction)
def prepare_onesite(A,direction):
    d,D1,D2 = A.shape 
    if direction=='lr':
        A = np.reshape(A,(d*D1,D2)) 
        B,S,U = svd2(A) 
        DB = S.shape[0]
        B = np.reshape(B,(d,D1,DB)) 
        U = np.dot(S,U)
        return B,U,DB
    elif direction=='rl':
        A = np.ndarray.transpose(A,1,0,2)
        A = np.reshape(A,(D1,d*D2)) 
        U,S,B = svd2(A)
        DB = S.shape[0]
        B = np.reshape(B,(DB,d,D2)) 
        B = np.ndarray.transpose(B,1,0,2)
        U = np.dot(U,S)
        return B,U,DB

#
# PREPARE CANONICAL FORM
#function [mps,U]=prepare(mps) 
def prepare(mps):
    N = len(mps)
    for ii in xrange(N-1,0,-1):
        mps[ii],U,DB = prepare_onesite(mps[ii],'rl')
        mps[ii-1] = np.tensordot(mps[ii-1],U,axes=(2,0))
    return mps,U
#
#GENERATE A RANDOM ORTHOGONAL MATRIX
#
def randO(n):
    X = np.random.randn(n,n)
    Q,R = linalg.qr(X)
    R = np.divide(np.diag(np.diag(R)),np.absolute(np.diag(R)))
    U = np.dot(Q,R)
    return U

#
#GENERATE A RANDOM MPS STATE WITH OBC
#mps(ii).shape=[d,Dl,Dr]
def createrandommps(N,D,d):
    mps = [None]*N
    mps[0] = np.random.randn(d,1,D)/np.sqrt(D)
    mps[-1] = np.random.randn(d,D,1)/np.sqrt(D)
    ind = 1
    while ind < N:
        u = randO(D*d)
        u = np.reshape(u,(d,D,d,D))
        u = np.ndarray.transpose(u,0,1,3,2)
        if ind+d-1<N-1:
            dd = -1
            for ii in xrange(ind,ind+d):
                dd = dd+1
                mps[ii] = u[:,:,:,dd]
        else:
            dd = -1
            for ii in xrange(ind,N-1):
                dd = dd+1
                mps[ii] = u[:,:,:,dd]
        ind = ind+d
    return mps
#
#function overlap=overlap(mps1,mps2)
#
#%evaluates <MPS1|MPS2> [hence the norm is sqrt(overlap)] for open boundary conditions
#%the complexity is O(N*D^3*d)
#%!!!!REMEMBER TO PROVIDE INPUT STATE THAT ARE NORMALIZED!!!!
#def overlap(mps1,mps2):
#    N1 = len(mps1)
#    d = mps1[0].shape[0]
#    overlap = np.reshape(1,(1,1,1))
#    X = np.reshape(np.eye(d,d),(1,1,d,d)) 
#    for j in xrange(N1-1,-1,-1):
#        overlap = updateCright(overlap,mps1[j],X,mps2[j]) 
#    return overlap[0,0,0]
def overlap(mps1,mps2):
    N1 = len(mps1)
    overlap = np.reshape(1,(1,1))
    for j in xrange(N1-1,-1,-1):
        overlap = np.tensordot(mps2[j],overlap,axes=(2,1)) 
        overlap = np.tensordot(mps1[j].conj(),overlap,axes=([0,2],[0,2]))
    return overlap[0,0]
#def overlap(mps1,mps2):
#    N1 = len(mps1)
#    overlap = np.reshape(1,(1,1))
#    for j in xrange(N1-1,-1,-1):
#        overlap = contracttensors(mps2[j],3,2,overlap,2,1) 
#        overlap = contracttensors(mps1[j].conj(),3,[0,2],overlap,3,[0,2])
#    return overlap[0,0]
#
#   APPLY MPO TO MPS EXACTLY 
#    
def mpomps(mpo,mps):
    N = len(mps)
    mps2 = [None]*N
    for indN in xrange(N):
        mpoD1,mpoD2,d,d = mpo[indN].shape
        d,mpsD1,mpsD2 = mps[indN].shape
        mps2[indN] = np.tensordot(mpo[indN],mps[indN],axes=(3,0))
        mps2[indN] = np.ndarray.transpose(mps2[indN],2,0,3,1,4)
        mps2[indN] = np.reshape(mps2[indN],(d,mpoD1*mpsD1,mpoD2*mpsD2))
    return mps2        
#
#function v = matvectn(L,R,mpox,v0)
#
def matvectn(L,R,mpox,v0):
    v = np.tensordot(L,v0,axes=(2,1))
    v = np.tensordot(v,mpox,axes=([1,2],[0,3]))  
    v = np.tensordot(v,R,axes=([2,1],[1,2]))    
    v = np.ndarray.transpose(v,1,0,2)
    return v    
#
#GENERATE MPO REPRESENTATION OF THE HAMILTONIAN FOR THE PHI4 MODEL
#
def mpohtil(mu,lamb,N,d):
    out = [None]*N
    mpophi = [None]*N
    mpophi2 = [None]*N
    id = np.eye(d,d)
    phi = np.zeros((d,d))
    phi2 = np.zeros((d,d))
    phi4 = np.zeros((d,d))
    p2 = np.zeros((d,d))
    for jp in xrange(d):
        for j in xrange(d):
            if j==jp:
                jv = j+1
                phi2[j,jp] = 0.5*(2.0*jv-1.0)
                phi4[j,jp] = 0.25*3.0*(2.0*jv**2.0-2.0*jv+1.0)
                p2[j,jp] = 0.5*(2.0*jv-1.0)
            if (j-1)==jp: 
                jv = j+1
                phi[j,jp] = (1.0/np.sqrt(2.0))*np.sqrt(jv-1.0)
            if (j-1)==(jp+1): 
                jv = j+1
                phi2[j,jp] = 0.5*np.sqrt((jv-1.0)*(jv-2.0));
                phi4[j,jp] = -0.25*6.0*np.sqrt((jv-1.0)*(jv-2.0));
                p2[j,jp] = -0.5*np.sqrt((jv-1.0)*(jv-2.0));
            if (j-1)==(jp+3): 
                jv = j+1
                phi4[j,jp] = 0.25*np.sqrt((jv-1.0)*(jv-2.0)*(jv-3.0)*(jv-4.0))
            if j==(jp+2):
                jv = j+1
                phi4[j,jp] = jv*np.sqrt((jv-1.0)*(jv-2.0))
            phi[jp,j] = phi[j,jp];
            phi2[jp,j] = phi2[j,jp]
            phi4[jp,j] = phi4[j,jp]
            p2[jp,j] = p2[j,jp]
    ope_loc = 0.5*p2 + 0.5*mu*phi2 + (lamb/(24.0))*(phi4)
#left border
    aux2 = np.zeros((1,3,d,d))
    aux2[0,0,:,:] = ope_loc+0.5*phi2
    aux2[0,1,:,:] = -phi
    aux2[0,2,:,:] = id
    out[0] = aux2
#right border
    aux3 = np.zeros((3,1,d,d))
    aux3[0,0,:,:] = id
    aux3[1,0,:,:] = phi
    aux3[2,0,:,:] = ope_loc+0.5*phi2
    out[-1] = aux3
#bulk
    for indN in xrange(1,N-1):
        aux = np.zeros((3,3,d,d))
        aux[2,:,:,:] = aux2[0,:,:,:]
        aux[:,0,:,:] = aux3[:,0,:,:]
        aux[2,0,:,:] = ope_loc+phi2
        out[indN] = aux
#%prepare the mpo for the phi observable
    del aux, aux2, aux3
#%left border
    aux2 = np.zeros((1,2,d,d))
    aux2[0,0,:,:] = phi
    aux2[0,1,:,:] = id
    mpophi[0] = aux2
#%right border
    aux3 = np.zeros((2,1,d,d))
    aux3[0,0,:,:] = id
    aux3[1,0,:,:] = phi
    mpophi[-1] = aux3
#%bulk
    for indN in xrange(1,N-1):
        aux = np.zeros((2,2,d,d))
        aux[1,:,:,:] = aux2[0,:,:,:]
        aux[:,0,:,:] = aux3[:,0,:,:]
        aux[1,0,:,:] = phi
        mpophi[indN] = aux
#%prepare the mpo for the phi2 observable
    del aux, aux2, aux3
#%left border
    aux2 = np.zeros((1,2,d,d))
    aux2[0,0,:,:] = phi2
    aux2[0,1,:,:] = id
    mpophi2[0] = aux2
#%right border
    aux3 = np.zeros((2,1,d,d))
    aux3[0,0,:,:] = id
    aux3[1,0,:,:] = phi2
    mpophi2[-1] = aux3
#%bulk
    for indN in xrange(1,N-1):
        aux = np.zeros((2,2,d,d))
        aux[1,:,:,:] = aux2[0,:,:,:]
        aux[:,0,:,:] = aux3[:,0,:,:]
        aux[1,0,:,:] = phi2
        mpophi2[indN] = aux
    return out,mpophi,mpophi2
#
#    function [mpsm,ee] = inner_loop(V1,mpo1,U1,mpsm,e0,m)
#
def inner_loop(V1,mpo1,U1,mpsm,e0,m):
    d,Dl,Dr = mpsm.shape
    vv = np.zeros((Dl*Dr*d,m+1))
    z1 = np.zeros((d,Dl,Dr,m))
    vv[:,0] = np.reshape(mpsm,(Dl*Dr*d))
    for ii in xrange(m):
        z1[:,:,:,ii] = np.reshape(vv[:,ii],(d,Dl,Dr))
        vv[:,ii+1] = np.reshape(matvectn(V1,U1,mpo1,z1[:,:,:,ii]),(Dl*Dr*d))-e0*vv[:,ii]
    zz, aux = linalg.qr(vv,mode='economic')
    mm = zz.shape[1]
    vv2 = np.zeros((Dl*Dr*d,mm))
    z2 = np.reshape(zz,(d,Dl,Dr,mm))
    for jj in xrange(mm):
        vv2[:,jj] = np.reshape(matvectn(V1,U1,mpo1,z2[:,:,:,jj]),(Dl*Dr*d))-e0*zz[:,jj]
    Am = np.dot(zz.conj().transpose(),vv2)
    eigval,eigvec = linalg.eigh(Am)
    mu = eigval[0]
    nu = eigvec[:,0]     
    ee = e0+mu
    ee = ee.real
    mpsm = np.reshape(np.dot(zz,nu),(d,Dl,Dr))
    return mpsm,ee
#    
#    function [mps_out,ee_out,list] = outer_loop(V1,mpo1,U1,mpsm,m_in,m_out,precision)
#
def outer_loop(V1,mpo1,U1,mpsm,m_in,m_out,precision): 
    list1 = np.zeros(100)
    mps = [None]*100
    list1[0] = np.tensordot(mpsm.conj(),matvectn(V1,U1,mpo1,mpsm),axes=([0,1,2],[0,1,2]))
    mps[0] = mpsm
    ii = 0
    for jj in xrange(m_out):
        ii = ii+1
        mps[ii],list1[ii] = inner_loop(V1,mpo1,U1,mps[ii-1],list1[ii-1],m_in)
    
    while ((np.std(list1[ii-m_out:ii])/abs(np.mean(list1[ii-m_out:ii])) > precision) & (ii<98)):
        ii = ii+1
        mps[ii],list1[ii] = inner_loop(V1,mpo1,U1,mps[ii-1],list1[ii-1],m_in)
    mps_out = mps[ii]
    ee_out = list1[ii]
    if list1[ii]>list1[0]:
        mps_out = mps[0]
        ee_out = list1[0]
    return mps_out,ee_out
#
#   SOLVE FOR THE GROUND STATE AND ENERGY OF THE PHI4 THEORY
#
def phi4(mps_in,N,D,d,lambt,mut02,iter_in,iter_out,precision,flag):
    if mps_in==[]:    
        mps_out = createrandommps(N,D,d)
        mps_out,U = prepare(mps_out)
        mps_out[0],U,DB = prepare_onesite(np.array(mps_out[0]),'rl')
    else:
        mps_out = mps_in[:]
    
    mpo,mpophi,mpophi2 = mpohtil(mut02,lambt,N,d)
    
    Renv = initenv(mps_out,mpo)
    Lenv = [None]*N
    sweep = 0
    while 1:
        sweep = sweep+1
        elist = []
        olist = []
        for ii in xrange(N-1):
            if ii==0: 
                Lenv[ii] = np.reshape(1,(1,1,1))
                
            mps_out[ii],ee = outer_loop(Lenv[ii],mpo[ii],Renv[ii],mps_out[ii],iter_in,iter_out,precision)
            mps_out[ii],U,DB = prepare_onesite(mps_out[ii],'lr')
            mps_out[ii+1] = np.tensordot(U,mps_out[ii+1],axes=(1,1))
            mps_out[ii+1] = np.ndarray.transpose(mps_out[ii+1],1,0,2)
            Lenv[ii+1] = updateL(mps_out[ii],Lenv[ii],mpo[ii])
            
#            op = overlap(mps_out,mpomps(mpophi,mps_out))
#            olist = np.append(olist,op)
            
            elist = np.append(elist,ee)
            if flag=='disp': print sweep,ii,ee
        for ii in xrange(N-1,0,-1):
            if ii==N-1: 
                Renv[ii] = np.reshape(1,(1,1,1))
            mps_out[ii],ee = outer_loop(Lenv[ii],mpo[ii],Renv[ii],mps_out[ii],iter_in,iter_out,precision)
            mps_out[ii],U,DB = prepare_onesite(mps_out[ii],'rl')
            mps_out[ii-1] = np.tensordot(mps_out[ii-1],U,axes=(2,0))
            Renv[ii-1] = updateR(mps_out[ii],Renv[ii],mpo[ii])
            
#            op = overlap(mps_out,mpomps(mpophi,mps_out))
#            olist = np.append(olist,op)
                        
            elist = np.append(elist,ee)
            if flag=='disp': print sweep,ii,ee
        if (np.std(elist)/np.mean(elist)<precision):
            op = overlap(mps_out,mpomps(mpophi,mps_out))
            op2 = overlap(mps_out,mpomps(mpophi2,mps_out))
            return mps_out,ee/N,op/N,op2/N
            break        
#
#THIS FUNCTION IS USED IN PHI4 THEORY TO RELATE BARE AND DRESSED PARAMETERS    
#    
def ctfunction(mut2,mut02,lambt):
    out = -mut02 + mut2 - (lambt/2.0)*1.0/(np.pi*np.sqrt(mut2+4.0))*special.ellipk(2.0/np.sqrt(mut2+4.0))
    return out
#
#THIS FUNCTION IS USED IN PHI4 THEORY TO LOCATE THE CRITICAL DRESSED PARAMETER    
#    
def mudressed(mut02,lambt):
    x0 = 1e-10
    out = optimize.fsolve(ctfunction, x0, args=(mut02,lambt))
    return out
#==============================================================================
# #THIS FUNCTION EVALUATE THE FIDELITY OF TWO STATES
#==============================================================================
def fidelity(mps1,mps2):
    return np.abs(overlap(mps1,mps2))
#==============================================================================
# #FIDELITY SUSCEPTIBILITY    
#==============================================================================
def susceptibilty(mps1,mps2,delta):
    return -2.0*np.log(fidelity(mps1,mps2))/(delta**2.0)
#==============================================================================
# THIS FUNCTION MERGES A BULK MPS INTO A LARGER CHAINWITH RANDOM BOUNDARIES
#==============================================================================
def merge_mps(mps0):
    import numpy as np
    
    N = len(mps0)
    d,Dl,_ = mps0[1].shape
    _,_,Dr = mps0[-2].shape
    
    mps_out = [None]*(N+2)
    mps_out[0] = mps0[0]
    mps_out[1] = mps0[1]
    mps_out[2:-2] = mps0[1:-1]
    mps_out[-2] = mps0[-2]    
    mps_out[-1] = mps0[-1]
    
    mps_out,_ = prepare(mps_out)
    mps_out[0],_,_ = prepare_onesite(np.array(mps_out[0]),'rl')

    return mps_out
    
