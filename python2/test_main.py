# -*- coding: utf-8 -*-
"""
Created on Thu Apr 10 16:19:00 2014

@author: sgarnero
"""
import tenet as tn
import numpy as np
from matplotlib.pylab import plot
from scipy.optimize import leastsq 

D = 2**3
d = 18
iter_in = 5;iter_out = 5;precision = 1e-7

lambt = 1.0
N0 = 20
numbM = 30 #mut02

mps0 = tn.createrandommps(N0,D,d)
mps0,_ = tn.prepare(mps0)
mps0[0],_,_ = tn.prepare_onesite(np.array(mps0[0]),'rl')

ee = np.zeros(numbM,)
b = np.zeros(numbM,)
bb = np.zeros(numbM,)
mut02 = [-0.45]
 
#==============================================================================
# loop over mut02 values
#==============================================================================
for ii in xrange(numbM):
    mps1,ee[ii],b[ii],bb[ii] = tn.phi4(mps0,N0,D,d,lambt,mut02[ii],iter_in,iter_out,precision,'nodisp')
    mps0 = mps1
    mut02 = np.append(mut02,mut02[ii]-0.25*1e-2)    
    print ii,mut02[ii],ee[ii],b[ii],bb[ii]
    
    
