function [out,mpophi] = mpohtil(mut02,lambt,N,d)
%
%this is the correct representation
%
out = cell(1,N);
mpophi = cell(1,N);
id=eye(d);
phi=zeros(d);
phi2=zeros(d);
phi4=zeros(d);
p2=zeros(d);
for jp=1:d
    for j=1:d
        if j==jp,
            phi2(j,jp)=0.5*(2*j-1);
            phi4(j,jp)=0.25*3*(2*j^2-2*j+1);
            p2(j,jp)=0.5*(2*j-1);
        end
        if (j-1)==jp, 
            phi(j,jp)=(1/sqrt(2))*sqrt(j-1);
        end
        if (j-1)==(jp+1), 
            phi2(j,jp)=0.5*sqrt((j-1)*(j-2));
            phi4(j,jp)=-0.25*6*sqrt((j-1)*(j-2));
            p2(j,jp)=-0.5*sqrt((j-1)*(j-2));
        end
        if (j-1)==(jp+3), 
            phi4(j,jp)=0.25*sqrt((j-1)*(j-2)*(j-3)*(j-4));
        end
        if j==(jp+2),
            phi4(j,jp)=j*sqrt((j-1)*(j-2));
        end
        phi(jp,j)=phi(j,jp);
        phi2(jp,j)=phi2(j,jp);
        phi4(jp,j)=phi4(j,jp);
        p2(jp,j)=p2(j,jp);
    end
end
ope_loc = 0.5*p2 + 0.5*mut02*phi2 + (lambt/24)*phi4;
%
%left border
%
aux2(1,1,:,:) = ope_loc+0.5*phi2;
aux2(1,2,:,:) = -phi;
aux2(1,3,:,:) = id;
out{1} = aux2;
%
%right border
%
aux3(1,1,:,:) = id;
aux3(2,1,:,:) = phi;
aux3(3,1,:,:) = ope_loc+0.5*phi2;
out{N} = aux3;
%
%bulk
%
for indN = 2:N-1
    aux=zeros(3,3,d,d);
    aux(3,:,:,:) = aux2(1,:,:,:);
    aux(:,1,:,:) = aux3(:,1,:,:);
    aux(3,1,:,:) = ope_loc+phi2;
    out{indN} = aux;
end
%
%prepare the mpo for the phi observable
%
clear aux aux2 aux3
%
%left border
%
aux2(1,1,:,:) = phi;
aux2(1,2,:,:) = id;
mpophi{1} = aux2;
%
%right border
%
aux3(1,1,:,:) = id;
aux3(2,1,:,:) = phi;
mpophi{N} = aux3;
%
%bulk
%
for indN = 2:N-1
    aux=zeros(2,2,d,d);
    aux(2,:,:,:) = aux2(1,:,:,:);
    aux(:,1,:,:) = aux3(:,1,:,:);
    aux(2,1,:,:) = phi;
    mpophi{indN} = aux;
end

