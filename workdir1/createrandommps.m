function [mps] = createrandommps(N,D,d)

mps=cell(1,N);
mps{1} = randn(1,D,d)/sqrt(D); 
mps{N} = randn(D,1,d)/sqrt(D); 
ind = 2;
while ind < N-1,
    u = randO(D*d);
    u = reshape(u,[D,d,D,d]);
    u = permute(u,[1 3 2 4]);
    if ind+d-1 < N-1,
        dd = 0;
        for ii = ind:ind+d-1
            dd = dd+1;
            mps{ii} = u(:,:,:,dd);
        end
    else
        dd = 0;
        for ii = ind:N-1
            dd = dd+1;
            mps{ii} = u(:,:,:,dd);
        end
    end
    ind = ind+d;
end
