function v = matvectn(L,R,mpox,v0)

v = contracttensors(L,3,3,v0,3,1);

v = contracttensors(v,4,[2,4],mpox,4,[1,4]);

v = contracttensors(v,4,[3,2],R,3,[2,3]);

v = permute(v,[1,3,2]);


