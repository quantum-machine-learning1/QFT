function out = updateR(mpsX,t,mpoX)
out = contracttensors(conj(mpsX),3,2,t,3,1);
out = contracttensors(out,4,[2,3],mpoX,4,[3,2]);
out = contracttensors(out,4,[4,2],mpsX,3,[3,2]);
