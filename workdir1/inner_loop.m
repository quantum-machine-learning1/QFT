function [mpsm,ee] = inner_loop(V1,mpo1,U1,mpsm,e0,m)

[Dl,Dr,d] = size(mpsm);

vv = zeros(Dl*Dr*d,m+1);
z1 = zeros(Dl,Dr,d,m);
vv(:,1) = reshape(mpsm,[Dl*Dr*d,1]);
for ii = 1:m
    z1(:,:,:,ii) = reshape(vv(:,ii),[Dl,Dr,d]);
    vv(:,ii+1) = reshape(matvectn(V1,U1,mpo1,z1(:,:,:,ii)),[Dl*Dr*d,1])-e0*vv(:,ii);
end
%
%find basis
%
[zz,~] = qr(vv,0);
% disp(norm(zz*zz'-eye(size(zz))))
%
%form the small pencil
%
mm = size(zz,2);
vv2 = zeros(Dl*Dr*d,mm);
z2 = reshape(zz,[Dl,Dr,d,mm]);
for jj = 1:mm
    vv2(:,jj) = reshape(matvectn(V1,U1,mpo1,z2(:,:,:,jj)),[Dl*Dr*d,1])-e0*zz(:,jj);
end

Am = zz'*vv2;
[nu,mu] = eigs(Am,1,'sr');
ee = real(e0+mu);
mpsm = reshape(zz*nu,[Dl,Dr,d]);

