function U = randO(n)

%    U = RANDU(n) generates a random  unitary matrix with n rows,
%    distributed uniformly according to the Haar measure.

% randn('seed',sum(100*clock));
% rng shuffle;
% RandStream.setGlobalStream(RandStream('mt19937ar','seed',sum(100*clock)));
X = randn(n);
[Q,R] = qr(X);
R = diag(diag(R)./abs(diag(R)));
U = Q*R;