function [mps,U]=prepare(mps) 
%preprare a R-canonical state
N=length(mps);

for ii=N:-1:2 
    [mps{ii},U]=prepare_onesite(mps{ii},'rl');
    mps{ii-1}=contracttensors(mps{ii-1},3,2,U,2,1);
    mps{ii-1}=permute(mps{ii-1},[1,3,2]); 
end


