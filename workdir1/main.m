clear all;
N = 512; 
D = 2^6; 
d = 2^4; 
lamb = 1;
numbp = 30;
lambt = lamb*0.6; %=lamb*a^2
mut02 = linspace(-0.35,-0.25,numbp);
svalue = zeros(numbp,1);
fide = zeros(numbp,1);
v = zeros(numbp,1);
delta = 1e-6;
mps0 = createrandommps(N,D,d);
mps0 = prepare(mps0);
[mps0{1},~]=prepare_onesite(mps0{1},'rl');

iter_in = 5;iter_out = 5;precision = 1e-6;
for ii = 1:numbp
    tic
    [mps1,ee,v(ii)] = phi4(N,D,d,mut02(ii),lambt,mps0,iter_in,iter_out,precision,'disp');
    toc
    tic
    [mps2,~,~] = phi4(N,D,d,mut02(ii)+delta,lambt,mps1,iter_in,iter_out,precision,'nodisp');
    toc
    fide(ii) = overlap(mps1,mps2);
%     disp([mut02(ii),ee,v(ii),fide(ii)]);
%     plotyy(mut02(1:ii),real(v(1:ii)),mut02(1:ii),real(fide(1:ii)));drawnow
%     plot(mut02(1:ii),v(1:ii),'-O');drawnow
    mps0 = mps2;
%     if ii==1,break;end
end

%     mps = cellpsi{ii};

% susceptibilty(cellpsi{1},cellpsi{ii},listm(1)-counterterm(listm(1),lamb)-listm(ii)+counterterm(listm(ii),lamb));
%     [mps2,~] = phi4(N,D,d,listm(ii)+delta-counterterm(listm(ii)+delta,lamb),lambt,mps1);

% svalue = zeros(size(listm,2)-1,1);
% for ii = 1:size(listm,2)
%         svalue(ii) = susceptibilty(cellpsi{1},cellpsi{ii},abs(listm(ii)-listm(ii+1)));
%         plot(listm(1:ii),real(svalue(1:ii)),'-x');drawnow
% end
% save('GS.mat','cellpsi');
% F = load('GS');

