function out = counterterm(mut2,lamb)

out = mut2-...
    (lambt/2)*(1/pi)*(1/sqrt(mut2+4)).*ellipke(2./sqrt(mut2+4));

xlist=linspace(0,1e-3,100);
lamb=0.1;
ylist = xlist-...
    (lamb/2)*(1/pi)*(1/sqrt(mut2+4)).*ellipke(2./sqrt(xlist+4));
plot(xlist,ylist)
